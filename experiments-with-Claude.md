# Claude Experiments

## Links

<!-----



Conversion time: 0.361 seconds.


Using this Markdown file:

1. Paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β35
* Wed Mar 20 2024 01:53:42 GMT-0700 (PDT)
* Source doc: Untitled document
----->


Here are some resources to get you started:



* [Developer documentation](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNJFshLfpo3ttazPNmUSSn4jBXc1_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5Eb2LJ94m1C2Tq081q41g2B3PsFXHA5lDOZyFmw0THQ95x1dmf1n4RDWxCtFuNYdIkCvUrejDGDz0z2609Y75fRDFNzLyVVSK7fvtiIcaKmrYbrylby2ps8nPc6rLdwAa-2BiOyRz7LaSVLJW0zRgVnjMew-3D-3D)
* [Prompt engineering resources guide](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNIQ67XNwo6WCIeiCHNt1dVdQjp8xNDsEFlfR9YZt36zFPPjWlR8B1lFd-2B-2B2mxLgTJtsB6LZSZqnzjKQHIuJamMgs1O1BOGxY1ZT2l-2Fztwmjcw-3D-3DZkF7_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5Ebmm18wniKlrgrPRFPVENybQVtpVTk2V-2B5zpOYoHBpha4gPXDy2GygL6aX9VBwkB2ok7CQnteNYQKOVLyNYW9jIOw5SSuE27rZ6MY5LkInDE2TlWhLcyytJutwFsTiIR7QkoYP-2BHORNvK6-2F9iY07vPgQ-3D-3D)
* [GPT to Claude migration guide](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNIQ67XNwo6WCIeiCHNt1dVd6mF4mZMyEvJCJrGYIx6Axnfvh17sHFfgdI6NmupI1eLxNclFSHnfEmPWh49fv6-2FCNlcS_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5EbpzbG1xKwD9ge4-2BN7TQpKsgWynwRJZ39EZMD0fQ2CmefIoebR-2Fpsha3ZQVA32KAgQ-2BTpSgTqtb4jMK-2BEB76JiGJeQUw032J8Cj1a0OoxRQlW2GzqGWmNcZ-2BvA4DRiZoqiTcQuA7FPm2ZSr1Jo4ulhIw-3D-3D)
* [Prompting for Claude’s long context window](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYb57odYuMXptiIvSnUIhs1OnVwi7-2FIiwWjIe3TMnxdISttM7OWIxqVLyzH5ujXDZXg-3D-3DMRO0_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5EbSIGg1dH8lyxLaKZxx297DpQDkHzaDvA5LRyLk8ycnFeUgg5H-2BsDXGKOo9V0DgIQs-2BfOT3eqh1SY3EnuzaRcKiguGLHjeX6DIOENnAmTBJ-2BmXRWZFhFroE57Us6IbRm6gpd6gl1ABOccC7ygjnz-2Bb9A-3D-3D)
* [Useful hacks for Claude](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNIQ67XNwo6WCIeiCHNt1dVdcmU31MaYIm97y10SRt4V-2FLL613tuC7BdbuxdxEEDtLU-3D4rmQ_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5Eb-2BXe0vJ98xteLDYiqyfq9HOSFJwkixFMi5VPYoTy9cWfs7F6cMptLJJRub-2FPw4jGaZftAsl76eYVTWeZs7095IfOic9esxtMEyozl6YXPiesT9CdRCEOqMedQ3SC31GYfTBP8guJ5WPZxDsmqykyvJA-3D-3D)
* [Use case examples](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNIQ67XNwo6WCIeiCHNt1dVd1dnJShItZVprSNWEPKLadt6N3-2FUTnfwY3fgCPTOtS3o-3DFn1t_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5Ebdm0r8Qcjl95-2BvMbvA8p-2BoG-2FMIdbBjk9vgFhfbgsClJii0sZ47cBHmx6exQMeT3pBJ-2BYbotg7d8ws-2BWWLbVB6hSwfQfmLRuVd-2BPBsPLj-2BNS7508Z6bE7sHmK4OxW6VUifhtvWMdK2I3hYjbXg0OHc-2FQ-3D-3D)
* [Trouble-shooting checklist](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYWBhJNd2KeeK-2FTTxcIQ1-2BNIQ67XNwo6WCIeiCHNt1dVdGFRU-2B17naA7a654sGcrvJGlwAEG6MAJb2VWzvuCpURM7JZhw7lfsQW5blXKoImT7cHVp_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5EbjsAGi6nkU1dz9KAtAtAtOu4UFkBH-2FHDePM-2FhgKEo6jDzAoq1y59U-2FwupV6PQFApII-2Bscm-2Byva2NZoz48uKMHGL9EmiTd8WR8IQbtP5lmykFQDCRgwHDOxJkqN-2Ft-2BE6V9S-2Bux1WyNKLtjRJhBxL-2BNlQ-3D-3D)
* [Anthropic Acceptable Use Policy](https://url8792.mail.anthropic.com/ls/click?upn=u001.rFcAmKXLOm9u6wLRWHIUYbEUju11Wua8clqbR4Eb2bLDfDD2FAmyIflsw8pRuqlq-2Fk-2BzwdP7CN534nrfibqK9Q-3D-3D6l5K_kp2RjQzARjH2WlFf1hQmQ32C6iHCUch8m1Ze8Pp9Z8pt0n47mepB9dJYDISxA5EbnlHeO3DFViVrqyDr-2FksKDAcVZUMz42xUwTmQ-2FYlkf0gRmeRhW9LAoOqH1CrVr8Ne9oq-2BmxmnubpHY6zo0U4YIZdV8M38USSUdnsf4bwI-2B6p6pkMn8GgxDW1FMBupuX4binLlKN2HqBSXnIM4YU56bw-3D-3D)

## Prompts

```
As an experienced and award-winning Customer Success Manager with expertise in creating customer-facing documentation and guidelines, your task is to create a comprehensive Customer Success Plan for enterprise customers using GitLab, the one DevSecOps Platform. The plan should be tailored to the specific needs of the customer, regardless of their industry vertical, including highly regulated sectors and public sector organizations.

To begin, gather the following information:



1. Customer name
2. URL of the customer's webpage

Conduct web research on the customer to better understand their business and value chains. Use the information gathered, along with your knowledge of the customer, to create a plan that includes:



1. A concise summary of the customer's business and key value chains
2. At least one clearly defined desired business outcome, focusing on the relevant GitLab Customer Value Drivers ([https://handbook.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers](https://handbook.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers))
3. Product adoption milestones necessary to achieve the stated outcome(s)
4. Measures of success for each milestone, including the baseline (current state) and desired state
5. Estimated dates for achieving each milestone

When crafting the Customer Success Plan, ensure that it is well-structured, easy to understand, and serves as a strong first iteration that can be refined with the customer's input. Incorporate the Questions & Techniques for Success Plan Discovery ([https://handbook.gitlab.com/handbook/customer-success/csm/success-plans/questions-techniques/](https://handbook.gitlab.com/handbook/customer-success/csm/success-plans/questions-techniques/)) to guide your information gathering and plan development.

Refer to the GitLab definition of a success plan ([https://handbook.gitlab.com/handbook/customer-success/csm/success-plans/](https://handbook.gitlab.com/handbook/customer-success/csm/success-plans/)) to ensure that your plan aligns with the company's standards and best practices.
```

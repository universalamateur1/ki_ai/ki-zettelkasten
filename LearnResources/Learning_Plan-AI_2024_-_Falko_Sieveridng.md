# Learning Plan AI 2024 - Falko Sieveridng

## Courses

- [x] [DeepLearning.AI - AI For Everyone - Coursera](https://www.coursera.org/learn/ai-for-everyone/)
  - Finished 18th Janaury 2024
- [ ] [DeepLearning.AI - Deep Learning Specialization - Coursera](https://www.coursera.org/specializations/deep-learning)

## To Read

- [ ] **[Why I Created Fabric](https://danielmiessler.com/p/fabric-origin-story)**: Daniel Miessler shares the motivation and development behind Fabric, an AI augmentation framework. **Value**: Insightful for understanding AI's potential in enhancing daily life and work.
- [ ] **[How to Fine-Tune LLMs in 2024 with Hugging Face](https://www.philschmid.de/fine-tune-llms-in-2024-with-trl)**: A detailed guide covering the end-to-end process of fine-tuning large language models with Hugging Face's tools. **Value**: High for developers and AI researchers interested in customizing or enhancing LLMs.
- [ ] **[Retrieval Augmented Generation (RAG) | Prompt Engineering Guide](https://www.promptingguide.ai/techniques/rag)**: Explains RAG's methodology for combining information retrieval with text generation to improve language models. **Value**: High for those enhancing AI applications with external knowledge access
- [ ] **[How I Use ChatGPT Daily: Scientist/Coder Perspective](https://bartwronski.com/2024/01/22/how-i-use-chatgpt-daily-scientist-coder-perspective/)**: Bart Wronski explains the utility of ChatGPT in his daily life, offering examples that showcase AI's practical benefits. **Value**: High for those looking to integrate AI into their workflows.
- [ ] **[How AI Will 100x Human Creativity and Output](https://danielmiessler.com/p/ai-will-100x-human-creativity-and-output)**: Daniel Miessler explains AI's potential to significantly enhance human creativity and productivity by solving execution challenges. **Value**: Insightful for understanding AI's transformative impact on creativity and output.

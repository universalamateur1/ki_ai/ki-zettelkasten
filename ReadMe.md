# KI Zettelkasten

The AI [Zettelkasten](https://de.wikipedia.org/wiki/Zettelkasten) in our GitLab group serves as a central repository for all permanent notes, personal journals and planning issues related to my AI projects. It's a dynamic and interconnected system that uses AI to organise and connect ideas, fostering deeper understanding and innovative solutions. This digital archive not only stores valuable information, but also intelligently suggests connections and insights, enhancing the creative process of AI experimentation. It's an essential tool for documenting progress, brainstorming and strategising, ensuring a cohesive and efficient workflow in our AI explorations.

- What can ML help with?

> ML is the System 1 of your brain if you follow "Thinking SLow and Fast" of Daniel K.. If a human can solve the taks within a few seconds or fast, you will be able to automise this with ML and supervised learning.

## Goals with this Project

I want to become an AI/KI/MLOps Poweruser, Solutionbuilder and get a foundational Understanmding of the technology enabling all of it.

## Suboages of Note

- [My Learning Plan for 2024](LearnResources/Learning_Plan-AI_2024_-_Falko_Sieveridng.md)
- [The Resource Registry of AI/KI/MLOps](resource_registry.md)

## Classes of AI

- Artificial General Intelligence (AGI).
- [ ] Add other Classes of AI.

## ML techniques

1. Supervised Learning
    - Input and Output is defined in the Training Data, even if it is several seperate Inputs. Learning A to B mappings, is the most valuable one, at least economically today.
        - Needs a lot of labeled data
1. Unsupervised Learning
    - Bunch of data into AI and it should find something interesting
      - Clustering Algorithm  
1. Transfer Learning
    - Learn from Task A, and use knowledge to help on task B. Great effect if the training set for task A was magnitudes bigger than that for taks B, but the tasks are still similar.
1. Reinforcement Learning
    - Incentive after shown behaviour was good, Punish on bad behaviour. Reward Signal high number, positive, which the AI tries to maximize, bad behaviour gives a negative number as reward signal. AlphaGo was trained in this way.
1. GANs (Generative Adversarial Network)
    - Given a training set, this technique learns to generate new data with the same statistics as the training set. In a GAN, two neural networks contest with each other in the form of a zero-sum game, where one agent's gain is another agent's loss. The core idea of a GAN is based on the "indirect" training through the discriminator, another neural network that can tell how "realistic" the input seems, which itself is also being updated dynamically.

## Major AI application areas

### Computer Vision

- Image classification/Object recognition - __Example:__ Face recognition
- Object detection - __Example:__ Self Driving Car
- Image segmentation - __Example:__ X-Ray analysis
- Tracking (in Videos) - __Example:__ autonomous Surveillance systems

### Natural Language Processing

- Text classification - __Example:__ Sentiment recognition
- Information retrieval - __Example:__ Web search
- Name entity recognition - __Example:__ Business or Surname in text
- Machine translation __Example:__ Written Japanese into english
- Parsing __Example:__ Identifying connected phraes and sentences
- Part-of-speech tagging __Example:__ Nouns, verbs, determinators and adjectives in language

### Speech

- Speech recognition - __Example:__ speech-to-text
- Trigger word/wakeword detection - __Example:__ "Hello Siri!"
- Speaker ID - __Example:__ Transcription service elemtn
- Speech synthesis (text-to-speech, TTS) - __Example:__ Accessibility assitances

ML test sentence (All letters present) `The quick brown fox jumps over the lazy dog.`

### Robotics

- Perception - __Example:__ figuring out what's in the world around you
- Motion planning - __Example:__ finding a path for the robot to follow
- Control - __Example:__ sending commands to the motors to follow a path

### General machine learning

- Unstructured data (images, audio, text) - fast to categorize, good ML use case
- Structured data - More complex, but much more valuable

## Classes of Models

- Domain-Specific Large Vision Models (LVMs)
- Large Language Models (LLM)
- [ ] Add other classes of Models
